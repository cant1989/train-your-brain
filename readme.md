# Train Your Brain
## Perchè

Lo scopo di questo progetto è elevare il confronto tra developers con l'intento di condividere e estendere le proprie conoscenze, sopratutto a livello di logica, approccio e tipi di pattern appicabili in differenti contesti di sviluppo.

Lavorare interamente e autonomamente sullo stesso progetto ci permette di avere un confronto più diretto e realistico, altrimenti non ottenibile nei task quotidiani dove ognuno ha i suoi, e difficilmete c'è la possibilità di confrontarsi sullo stesso identico task.

## Istruzioni

1. Clona il progetto
2. Crea un branch col tuo nome
3. Ogni cartella è un'esercizio con un proprio readme, fai quello/i che preferisci
4. Quando hai finito fai una pull request